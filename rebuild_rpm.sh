#!/bin/bash
# Usage: . ./rebuild_rpm.sh dir

mkdir -p "$1/rpmbuild"

# Custom spec file...
if [ -f $1/custom/*.spec ]
then
  echo "(we are using a custom spec file)"
  rpm --define "_topdir $PWD/$1/rpmbuild" -i $1/*.src.rpm
  cp -f $1/custom/*.spec $1/rpmbuild/SPECS
  yum-builddep -q -y $1/rpmbuild/SPECS/*.spec
  spectool -g -C $1/rpmbuild/SOURCES $1/rpmbuild/SPECS/*.spec
  linux32 rpmbuild --quiet --target=i686 --define "_topdir $PWD/$1/rpmbuild" --define "dist .el7" --define "arch i686" --ba $1/rpmbuild/SPECS/*.spec
# ...or just rebuild
else
  yum-builddep -q -y $1/*.src.rpm
  linux32 rpmbuild --quiet --rebuild --target=i686 --define "_topdir $PWD/$1/rpmbuild" --define "dist .el7" --define "arch i686" --ba $1/*.src.rpm
fi
